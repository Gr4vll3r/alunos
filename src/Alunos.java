import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Alunos {
	private static String[] alunos = { "Laura", "Beatriz", "Maria", "Ana", "J�lia", "Alice", "Mariana", "Larissa",
			"Sofia", "Maria_Eduarda", "Isabela", "Helena", "Camila", "Lara", "Valentina", "Let�cia", "Luana", "Amanda",
			"Yasmim", "Sophia", "Rebeca", "Pedro", "Juliano", "Bruna", "L�via", "Vit�ria", "Miguel" };

	public static void main(String[] args) {
		List<String> diasSemana = new LinkedList<>();
		diasSemana.add("Segunda");
		diasSemana.add("Ter�a");
		diasSemana.add("Quarta");
		diasSemana.add("Quinta");
		diasSemana.add("Sexta");

		for (String semana : diasSemana) {

			System.out.print(semana + " | ");
		}
		System.out.println();
		System.out.println();

		loopingAlunos();// Alunos + notas
		System.out.println();
		loopingAlunos();
		System.out.println();
		loopingAlunos();
		System.out.println();
		loopingAlunos();
		System.out.println();
		loopingAlunos();

	}

	private static int[] number = { 2, 1 };

	public static void loopingAlunos() {
		String verificar;
		for (int i = 0; i < alunos.length - 23; i++) {
			if (number[new Random().nextInt(number[0])] % 2 == 0) {
				verificar = "+1";
			} else {
				verificar = "-1";
			}
			System.out.print(alunos[new Random().nextInt(alunos.length)] + ": ".concat(verificar + "   "));

		}
	}

}
